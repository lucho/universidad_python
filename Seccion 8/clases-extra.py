class Persona:
    
    def __init__(this,n,e,*valoresTupla,**diccionario):
        this.nombre = n
        this.edad = e
        this.valores = valoresTupla
        this.diccionario = diccionario
    
    def desplegar(this):
        print('El nombre es: ',this.nombre)
        print('Edad:', this.edad)
        print('Valores de la tupla: ',this.valores)
        print('Valores del diccionario: ', this.diccionario)
        
    
    
persona = Persona('Lucia',34)

persona.desplegar()

#print(persona.nombre)
#print(persona.edad)
print('#######################################')
persona2 = Persona('Lucho',34,2,32,4,456,5)

persona2.desplegar()


print()

persona3 = Persona('Sofia',34,32,3242,23,4,32, nombre='Paola', sueno='otro país')
persona3.desplegar()