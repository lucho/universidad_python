class Rectangulo:
    
    def __init__(self,base,altura):
        self.base = base
        self.altura = altura
    
    def calcular_area(self):
        return self.base * self.altura
    

base = float(input('Proporcione la base: '))
altura = float(input('Proporcione la altura: '))    

rectangulo = Rectangulo(base,altura)

print('El área es: ', rectangulo.calcular_area())