class Caja:
    
    def __init__(self,alto,ancho,largo):
        self.alto = alto
        self.ancho = ancho
        self.largo = largo
    
    def calcular_volumen(self):
        return self.alto * self.ancho * self.largo
    
    

alto = float(input('Proporcione el Alto: '))
ancho = float(input('Proporcione el Ancho: '))
largo = float(input('Proporcione Largo: '))


caja = Caja(alto,ancho,largo)

print('El volumen de la caja es : ', caja.calcular_volumen())
