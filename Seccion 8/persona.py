class Persona:
    
    def __init__(self,nombre,edad):
        self.nombre = nombre
        self.edad = edad

#Modificar los valores
Persona.nombre = 'Luis'
Persona.edad = 23

#Acceder a los valores
print(Persona.nombre)
print(Persona.edad)

#Crear una nueva instancia del objeto Persona

persona = Persona('Natalia',23)
print(persona.nombre)
print(persona.edad)

#Creación de un segundo objeto

persona2 = Persona('Carlos', 40)

print(persona2.nombre)
print(persona2.edad)
print(id(persona2))
print(id(persona.nombre))

