class Aritmetica:
    """Clase Aritmetica,para realizar las operaciones de suma, resta etc..."""
    def __init__(self,operando1,operando2):
        self.operando1 = operando1
        self.operando2 = operando2
    
    def sumar(self):
        """See realiza la peración con los atributos de la clase"""
        return self.operando1 + self.operando2
    
# Crear nuevo objeto de aritmetica

aritmetica = Aritmetica(2,3)
print('Resultado de la suma es. ',aritmetica.sumar())