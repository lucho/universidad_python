# Imprimir solo las letras 1 contenidas en una cadena con break se rompe el sicro

for letra in 'Holanda':
    if letra == 'a':
        print(letra)
        break
else:
    print('Sin sicro for')
    
print('COntinua el programa.')

#Ejercio, imprimir solo los numeros pares de un rango

# for i in range(6):
#     if i %2 == 0:
#         print(i)    

for i in range(6):
    if i %2 != 0:
        continue #continue indica que se siga siendo ejecutando el proceso
    print(i)        
