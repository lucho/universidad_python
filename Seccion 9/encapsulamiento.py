# Encapsulamiento basicamente es que no puedan acceder a los atribustos de nuestra clase

class Persona:
    
    def __init__(self, nombre):
        self.__nombre = nombre
    
    def get_nombre(self):
       return self.__nombre
    
    def set_nombre(self,nombre):
        self.__nombre = nombre
        
        
        

persona = Persona('Juan')

print(persona.get_nombre())

persona.set_nombre('Karla')

print(persona.get_nombre())
