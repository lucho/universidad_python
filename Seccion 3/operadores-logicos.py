a = int(input('Indica el valor: '))
valorMinimo = 0
valorMaximo = 5
dentroDeRango = (a >= valorMinimo and a <= valorMaximo)
if dentroDeRango:
    print('Si esta dentro del rango')
else:
    print('No se encuentra en el rango')
    
#Ejemplo 2

vacaciones = True
diaDescanso = False

if vacaciones or diaDescanso:
    print('Podemos ir al Parque')
else:
    print('Tenemos deberes que hacer')
    
#Ejemplo 3 not, Invierte en valor voleanod True = False, False = True

print(not(vacaciones))

if not(vacaciones or diaDescanso):
    print('Podemos ir al Parque')
else:
    print('Tenemos deberes que hacer')