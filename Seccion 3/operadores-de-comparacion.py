a = 3
b = 3

resultado = a == b
print(resultado)

resultado = a != b
print(resultado)


resultado = a > b
print(resultado)

resultado = a < b
print(resultado)

resultado = a >= b
print(resultado)

# Mirar si a es un numero par, para esto se mira si es divisible entre 2

if a%2 == 0:
    print('Si es un numero par')
else:
    print('No es un numero par')

#Mirar si una persona es mayor de edad

edadLimite = 18
edadPersona = 19

if edadPersona >= edadLimite:
    print('Si la persona es mayor')
else:
    print('No la persona no es mayor')