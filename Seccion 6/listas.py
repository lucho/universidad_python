nombres = ['Juan','Karla','Ricardo','Maria']

print(nombres[0])
#Conocer el largo de la lista
print(len(nombres))
#navegacion inversa, indices nevatios

print(nombres[-1])
print(nombres[-2])

#parte 2
#Imprimir rango

print(nombres[0:2]) # Sin incluir el indice 2

#Imprimir los elementos de inicio hasta el indice proporcionado

print(nombres[:3])

#Impirmir los elementos hasta el final, desde el indice proporcionado

print(nombres[1:]) # desde la posicion 1 hasta el final, sin tener presente el indice 0

# Cambiar los elementos de una lista

nombres[3] = 'Ivone'
print(nombres)

#Iterar una lista
for nombre in nombres:
    print(nombre)
    
#Rebisar si existe un nombre
if 'Karla' in nombres:
    print('Si existe en la lista')
else:
   print('No existe en la lista')
   
#Parte tres

#append agregar un nuevo elemento en el ultimo lugar

nombres.append('Lorenzo')
print(nombres)
# Insertar nuevo elemento en el indice proporcionado

nombres.insert(1,'Octavio')
print(nombres)
#Eliminar un elemento

nombres.remove('Octavio')
print(nombres)

#Remover el ultimo elelemnto de la lista

nombres.pop()
print(nombres)

#remover un indice indicado de la lista

del nombres[0]
print(nombres)

#Limpiar los elelemtos de la lista

nombres.clear()
print(nombres)
#Eliminar toda la la lista, incluso elimina la variable

del nombres
print(nombres)
    



