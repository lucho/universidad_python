#Una tupla, mantiene el orden, pero ta no se puede modificar. Colección inmutable
frutas = ('Naranja','Platano', 'Guayaba')
print(frutas)
#Largo de la lista
print(len(frutas))
#Acceder a un elemento
print(frutas[0])
#Navegación inversa
print(frutas[-1])
#Rango
print(frutas[0:2])
#Modificar un valor
#frutas[0]="Naranjita" #No se puede modificar

frutasLista = list(frutas) # Convertir de tuplas a lista
frutasLista[1] = "Platanito"
frutas = tuple(frutasLista) # Se hace la conversion de la lista a tupla

print(frutas) 

#Iterar una tupla

for fruta in frutas:
    print(fruta,end=",")

# No podemos ni agregar elementos ni cambiar eleimentos de un tupla

del frutas
print(frutas)
