#Un diccionario esta compuesto por llave, valor (key,value)
diccionario = {
    'IDE': 'Integrated Development Environment',
    'OOP': 'Objet Oriented Programming',
    'DBMS': 'Database Management System'
}

print(diccionario)

#Largo

print(len(diccionario))

# Acceder  un elemento

print(diccionario['IDE'])

# Otra forma, de acceder, es utilizando la función get

print(diccionario.get('IDE'))

# Modificando valores

diccionario['IDE'] = 'Integrated development environment'

print(diccionario)

#Iterar
for termino in diccionario: 
    print(termino)
#Acceder a los valores

for termino in diccionario:
    print(diccionario[termino])

for valor in diccionario.values():
    print(valor)
    
# comprobar si existe un elemnto

print('IDE' in diccionario)

#Agregar un nuveo elemento

diccionario['APK'] = 'Primary Key'

print(diccionario)

#Remover elementos del diccionario con pop
diccionario.pop('DBMS')

print(diccionario)

#Limpiar 
diccionario.clear()
print(diccionario)

#Eliminar por completo el diccionario(Incluso la variable)
del diccionario

print(diccionario)
    