# Set es una coleecion sin orden,y tampoco tienen indices,
# No permite elelementos repetidos, pero si agregar nuevos o eliminar

planetas = {'Marte','Jupiner','Venus'}
print(planetas)

# Largo
print(len(planetas))
#Revisar si un elemento está presente.

print('Marte' in planetas)

# Agregar
planetas.add('Tierra')

print(planetas)

planetas.add('Tierra')
print(planetas)

#Eliminar
#planetas.remove('Tierra')
print(planetas)

# el metodo discard no arroja error.
planetas.discard('Tierras')
print(planetas)

# Lipiar por completo.
planetas.clear()
print(planetas)

#Eliminar el set

del planetas
print(planetas)
