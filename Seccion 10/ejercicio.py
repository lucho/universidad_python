class Vehiculo:
    
    def __init__(self,color,ruedas):
        self.color = color
        self.ruedas = ruedas
        
    def __str__(self):
        return 'Color: ' + self.color + ' Ruedad: '  + str(self.ruedas)


class Coche(Vehiculo):
    
    def __init__(self,color,ruedas,velocidad):
        super().__init__(color,ruedas)
        self.velocidad = velocidad
        
    def __str__(self):
        return super().__str__() + ' Velocidad: ' + self.velocidad
    
class Bicicleta(Vehiculo):
    
    def __init__(self,color,ruedas,tipo):
        super().__init__(color,ruedas)
        self.tipo = tipo
        
    def __str__(self):
        return super().__str__() + ' Tipo: ' + self.tipo
    
    

vehiculo = Vehiculo('Rojo',4)
print(vehiculo)

coche = Coche('Azul',5,'24 km')
print(coche)

bicicleta = Bicicleta('Amarillo',2,'urbana')

print(bicicleta)

        