
class Persona:
    
    def __init__(self,nombre,edad):
        self.nombre = nombre
        self.edad = edad
        
    def print_atributos(self):
        print(self.nombre)
        print(self.edad)
    
    def __str__(self):
        return 'Nombre:' + self.nombre + ' Edad: ' + str(self.edad)
        

class Empleado(Persona):
    
    def __init__(self,nombre,edad,sueldo):
        super().__init__(nombre,edad)
        self.sueldo = sueldo
        
    def __str__(self):
        return super().__str__() + ' Sueldo: ' + str(self.sueldo)
        
   
        
        


persona = Persona('Luis',22)
print()
print(persona)
print()
empleado = Empleado('Angie',23,'1.000.000')

print(empleado)

print(persona.nombre)
print(persona.edad)
print()
print(empleado.nombre)
print(empleado.edad)
print(empleado.sueldo)
print()
print(empleado.print_atributos())